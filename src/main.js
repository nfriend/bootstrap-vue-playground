import Vue from 'vue';
import { BVConfig } from 'bootstrap-vue';
import App from './App.vue';

Vue.config.productionTip = false;

Vue.use(BVConfig, {
  BTooltip: {
    delay: {
      show: 1000,
      hide: 1000,
    },
  },
  BPopover: {
    delay: 1000,
  },
});

new Vue({
  render: h => h(App),
}).$mount('#app');
