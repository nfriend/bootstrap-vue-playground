const path = require('path');

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        vue: path.join(__dirname, 'node_modules/vue'),
        'bootstrap-vue$': path.join(
          __dirname,
          'node_modules/bootstrap-vue/src/index.js',
        ),
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "node_modules/bootstrap/scss/bootstrap";
          @import "node_modules/bootstrap-vue/src/index.scss";
        `,
      },
    },
  },
};
